using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    public int coinValue = 1;
    public Animator cofreabre;
    public bool Cofabierto;
    // Start is called before the first frame update
    void Start()
    {
        //Cofabierto = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
            if (collision.GetComponent<Collider2D>().CompareTag("Player"))
            {
                if (Input.GetKey(KeyCode.W))
                {
                    if (Cofabierto == false)
                    {
                        cofreabre.SetBool("open", true);
                        contadordemonedas.SumaMoneda(coinValue);
                        Cofabierto = true;
                    }
                }
            }
        }
    }


               