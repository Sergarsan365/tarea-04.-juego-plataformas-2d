using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Andar : MonoBehaviour
{
    public float vel = 0.01f;
    public Animator AndarDer;
    public SpriteRenderer personajeSprite;
    public float salto = 0.1f;
    public float JumpFrecuency;
    private float lastJump;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void Update()
    {
        AndarDer.SetBool("Andar", false);
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * vel * Time.deltaTime;
            AndarDer.SetBool("Andar", true);
            personajeSprite.flipX = true;


        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * vel * Time.deltaTime;
            personajeSprite.flipX = false;
            AndarDer.SetBool("Andar", true);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.time - lastJump >= JumpFrecuency)
            {
                rb.velocity = Vector2.up * salto;
                lastJump = Time.time;
            }
        }
        //else
        //{
        //    AndarDer.SetBool("Andar", false);
            
        //}
        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    transform.position += Vector3.left * vel;
        //}
    }
}
